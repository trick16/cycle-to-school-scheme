#!/usr/bin/env bash

python3 -m http.server --bind "localhost" --directory ./output &
PID=$!
trap 'kill $PID' INT
make watch
