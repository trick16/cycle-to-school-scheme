all: site

dev:
	bash ./dev.sh
watch:
	while true; do \
		make -j 24 site; \
		inotifywait -qre close_write ./src; \
	done

site: site.image site.style site.org
site.image:
	rm -r ./output/img;
	cp -r ./src/img ./output/img;
site.style:
	sassc ./src/scss/main.scss ./output/style.css
site.org:
	emacs --batch --script make.el --kill

wordcount:
	fdfind -e ".org" . ./src/org | xargs wc -w

deploy.copy:
	cp -r ./output/* ../trick16.com/public/project/ctss
	rm ../trick16.com/public/project/ctss/report.html~
deploy.message:
	@echo "`tput setaf 125`You want to commit and push the trick16 site to deploy/master now! `tput sgr0`"
deploy: site deploy.copy deploy.message

.PHONY: clean
clean:
	rm -r ./output/*
