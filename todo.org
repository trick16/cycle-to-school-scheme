* Site
** TODO improve styling
*** DONE Look at tecosaur implementation
This blog has a lot of nice improvements to the html export that we should think about using: https://tecosaur.github.io/emacs-config/config.html#exporting-html
*** DONE Use scss from tecosaur implementation to bootstrap new style
*** TODO responsive images
*** TODO more modern hipster style
Sepia-ish?
** TODO Separate headings into different exported files?
We should be able to split arbitrary levels into new files... or even like =:individual-page:= tagged headings...
** TODO "target" style concentric circles showing speed limits and density of student body
Target style student catchment area plotted on to map of streets with speed limits. Aim to highlight  safety and potentially suggest increasing the 30km speed limit near schools
