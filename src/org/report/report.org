#+TITLE: Cycle to School Scheme Report
#+HTML_DOCTYPE: html5
#+HTML_HEAD_EXTRA: <link rel="stylesheet" type="text/css" href="style.css" />
#+HTML_HEAD_EXTRA: <script type="text/javascript" src="https://unpkg.com/zooming"></script>
#+HTML_HEAD_EXTRA: <script type="text/javascript">document.addEventListener('DOMContentLoaded', function () {new Zooming({scaleBase: 0.7}).listen('img:not(.logo-fixed):not(.logo)')})</script>
#+EXPORT_FILE_NAME: ../../../output/report.html
#+OPTIONS: H:10 toc:nil html-postamble:nil html5-fancy:t
#+BEGIN_COMMENT
#+HTML_HEAD_EXTRA: <script type="text/javascript" src="http://livejs.com/live.js"></script>
#+END_COMMENT


#+BEGIN_subtitle
An exploration of a hypothetical government scheme, using open data to create rich visualisations and reach interesting conclusions.
#+END_subtitle

#+BEGIN_EXPORT html
<a href="https://trick16.com">
<img class="logo-fixed" src="./img/logo.png"/>
</a>
#+END_EXPORT

#+TOC: headlines 2

* Introduction
** What is this document
This document is a study of a prospective government scheme, the /Cycle to School Scheme/, including sections on feasibility and on potential future outcomes of the scheme.

All visualisations, studies, text, and data used in this report are published under open licenses, enabling re-use of any part of this document. The methodology and source code used to create this document are also published under an open license.

The source code for this document can be found at https://gitlab.com/trick16/cycle-to-school-scheme

The source code is released under the BSD 2-Clause open license, except for the CSS for this website, which is released under the GPL 3.0.

The visualisations and text within this document are released under Creative Commons Attribution (4.0), copyright Carl Lange.

Data used in the making of this document is copyright their original copyright holder, listed in the [[file:./report.org#Appendix][Appendix]].

** Statement of the scheme
The Cycle To School Scheme is a hypothetical proposed scheme to supply bicycles - one bicycle per student - to all secondary schools in the Republic of Ireland. The intent of this scheme is to increase mobility and exercise among all socio-economic groups of secondary school students, as well as decreasing Ireland's carbon footprint by requiring fewer car journeys to and from schools.

** The goal of this report
This report is intended as a research study, determining how feasible the prospective Cycle To School Scheme would be to implement, as well as charting some of the potential outcomes of the scheme. The main question this report is intended to answer is: Should the Cycle To School Scheme be implemented, and if so, how?

We aim to explore many aspects of the scheme in a readable and understandable manner using data analytics and static and dynamic visualisations. The report is not exhaustive or particularly rigorous, but we hope that it inspires interest in a scheme like the Cycle to School Scheme. We also hope that this report, and the workflow we used to create it, inspires future open civic research and interest in open data.

Open Data has opened the door for a significant amount of citizen participation in research, by enabling anybody to access, re-use, and re-distribute source data. This document is intended as an example of the kind of research that can now be done by any citizen. The publication of government data as open data has removed a really sizeable "data origination" step from the process of research, particularly where the data is published in machine-readable formats. We hope to demonstrate some of the power of this data with this report.

** Acknowledgements
Data for this report comes primarily from data.gov.ie, Ireland's Open Data Portal. A list of used datasets is included in the appendix, which also includes relevant datasets that we did not end up using directly.

Map data used is from Open Street Maps and used under the Open Database License.

This report was funded by the 2020 Open Data Engagement Fund.

The producers of this report are Carl Lange, Jen Carey, and Shane McDonagh, for Trick16. Trick16 is a creative technical consultancy in Ireland aiming to bring bespoke technical solutions to the Arts, Culture, Heritage sector.

Chief author: Carl Lange

Assistant author: Shane McDonagh

Producer: Jen Carey

Finally, a big thank you to everyone involved in open data in Ireland!

* Methodology
** How was the document created?
This document is written primarily in org-mode, an open source, plaintext format for writing. The use of this format over proprietary formats such as Microsoft Word ensures that the text of the report is accessible to as many people as possible, and allows the report to be "source-controlled" - different revisions of the document are stored and retrieved, and the entire history of the document is available.

This org-mode document is converted into HTML, the format used for web pages, to publish the document on the internet.

Research was primarily done within Wolfram Mathematica, a proprietary programming language, document format, and toolkit. Mathematica is commonly used for scientific research, and has an extremely large feature-set, enabling speedy research workflows. The use of a proprietary format for the research was a trade-off based on the ease of research. However, the Wolfram Notebooks are published alongside all other source material. Everything done within Mathematica can be recreated using an open-source programming language such as Python or R, and the Wolfram Notebooks are published to facilitate the verification of research results in arbitrary programming languages. Diagrams and visualisations have been created by Wolfram Mathematica, except where otherwise stated.

We structured the process of research around building blocks called "ARC"s - "Ask, Research, Conclude" blocks, which then built up larger sections of research over the course of a "sprint", typically 3-4 weeks in length.

An ARC section is typically a standalone data exploration. We "ask" a question (for example, "How many bicycles would we need?"), "research" the answer based on several data sources, exploring the data and creating visualisations and prose, and then "conclude" the result.
We did monthly reviews of the questions we had lined up to explore using an ARC block, and usually did one ARC per week, although the sections often progressed asynchronously. Typically at the end of each month, we had a writing week, where the results of each ARC was drafted into the report itself. Then, a new set of questions for ARC blocks were originated, and another "sprint" was begun.

** Structure of this document
We structured this document to contain several "Research and Outcomes" sections, based around our ARC blocks (described in the previous section). We intend the Research and Outcomes sections to be standalone data explorations, which contribute to the overall goal of determining the feasibility of the Cycle to School Scheme. They can be read in any order, more or less.

Typically, a Research and Outcomes section is structured by:
- stating the problem, hypothesis, or question
- an approximate methodology and the datasets we expect to use
- the body of research, often including specific code snippets and visualisations
- a conclusion, ideally solving, proving, or answering the problem stated in the beginning

* Research and Outcomes
#+INCLUDE: "./arc/how-many-bikes.org" :only-contents t
#+INCLUDE: "./arc/how-expensive.org" :only-contents t
#+INCLUDE: "./arc/how-many-schools-are-feasible.org" :only-contents t
#+INCLUDE: "./arc/how-many-km.org" :only-contents t
#+INCLUDE: "./arc/traffic-due-to-journeys-to-school.org" :only-contents t
#+INCLUDE: "./arc/size-of-bicycle-industry.org" :only-contents t
#+INCLUDE: "./arc/accidents.org" :only-contents t

* Conclusion
** Research Outcomes
- Approximately 370,000 bicycles would be needed to cover all secondary schools in Ireland at any point.
- Assuming 10% loss per year, over the first 10 years of the scheme 700,000 bicycles would need to be supplied.
- A little over one quarter of schools are within 5km of a bicycle shop, enabling easier maintenance of stock
- About one quarter of schools might be ready for the scheme
- there are somewhere between 200 and 500 bike shops in Ireland, indicating a reasonably healthy industry
- the creation of bicycle-related infrastructure is growing significantly
- Peak accident times involving cyclists are 0800 to 0900 and 1600 to 1800
- The vast majority of students driving to school leave home between 07:30 and 09:00.
- Government funding already exists to improve cycling and walking infrastructure.
- A case study of Strandhill National School's Cycle Bus suggests that accidents are unlikely in rural or semi-rural locations, especially with adult supervision.
- Training providers are already established in the field. Working with training providers would likely lower the safety risk.
- We estimate that about 3.6 million kilometres are driven to and from school every day in Ireland.
- The initial cost of the full scheme for secondary school students might cost around 80,000,000 euros.
- A pilot scheme supplying 5% of secondary school students might cost around 5,000,00 Euro.

The list of questions we did not get to answer is extremely lengthy. There are a lot of long-term effects a scheme like the Cycle to School Scheme might have, but unfortunately we didn't get to discuss any of those in depth.
Finally: we are not scientists, and our results should not be seen as comprehensive, exhaustive, or scientifically rigorous. If you happen to base a multi-million euro government scheme based on this document alone, that's on you.

* Appendix
** Code attributions
Org-mode CSS theme by Abhinav Tushar, licensed under GPL 3.0. ([[https://github.com/lepisma/pile-theme][Original source]]; [[https://gitlab.com/trick16/cycle-to-school-scheme/-/tree/master/src/scss][edited source]])

Org-mode anchor links helper by alphapapa, licensed under GPL 3.0. ([[https://github.com/alphapapa/unpackaged][Original source]]; [[https://gitlab.com/trick16/cycle-to-school-scheme/-/tree/master/src/elisp/make.org][edited source]])

Javascript image zoom library by Desmond Ding, licensed under MIT. ([[https://github.com/kingdido999/zooming][Source]])

=htmlize= by Hrvoje Nikšić, licensed under GPL 3.0. ([[https://github.com/hniksic/emacs-htmlize][Source]])

=dash.el= by Magnar Sveen, licensed under GPL 3.0. ([[https://github.com/magnars/dash.el][Source]])

=sparql-mode= by Bjarte Johansen, licensed under GPL 3.0. ([[https://github.com/ljos/sparql-mode][Source]])

=wolfram-mode= by kawabata, licensed under GPL 3.0. ([[https://github.com/kawabata/wolfram-mode][Source]])

** Master list of data sources
#+INCLUDE: "./appendix/data-source-master-list.org" :only-contents t
** Sophox, a linked data store for Open Street Maps data
#+INCLUDE: "./appendix/linked-data-store-for-ireland.org" :only-contents t

#+BEGIN_EXPORT html
<div id="footer">
<hr/>
<p>Thanks for reading!</p>
<p class="copyright">© Trick16 2021</p>
<a href="https://trick16.com">
<img class="logo-bottom" src="./img/logo.png"/>
</a>
</div>
#+END_EXPORT
